﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ExtrudableTexture : System.IDisposable
{
    private GameObject container;
    private SpriteRenderer utilitySpriteRenderer;
    private PolygonCollider2D polygonCollider;

    private ExtrudableTexture(){}

    public ExtrudableTexture(Texture2D texture)
    {
        container = new GameObject("Utility Sprite Renderer");
        if(GameObject.Find("Runtime") is var runtime && runtime!=null)
            container.transform.SetParent(runtime.transform);
        utilitySpriteRenderer = container.AddComponent<SpriteRenderer>();
        utilitySpriteRenderer.sprite = CreateSprite(texture);
        polygonCollider = utilitySpriteRenderer.gameObject.AddComponent<PolygonCollider2D>();
    }

    public List<Vector2[]> GetPathes()
    {
        var pathList = new List<Vector2[]>();
        for(int i = 0;i < polygonCollider.pathCount;i++)
            pathList.Add(polygonCollider.GetPath(i));
        return pathList;
    }

    public Vector2[] GetMainPath()
    {
        return GetPathes().ToList().OrderByDescending(path => path.Length).First();
    }

    public Vector2 GetPosFromUVCoord(Vector2 uvCoord)
    {
        var localExtets = utilitySpriteRenderer.bounds.size;
        var extentsUV = uvCoord - Vector2.one*0.5f;
        float x = localExtets.x * extentsUV.x;
        float y = localExtets.y * extentsUV.y;
        return new Vector2(x,y);
    }

    public Vector2 GetSpriteCoordinate(Vector2 point)
    {
        var localExtets = utilitySpriteRenderer.bounds.extents;
        var flatExtets = new Vector2(localExtets.x, localExtets.y);
        return ((point / flatExtets) + Vector2.one)*0.5f;
    }

    private Sprite CreateSprite(Texture2D source)
    {
        var spriteRect = new Rect(0, 0, source.width, source.height);
        var pivot = Vector2.one * 0.5f;
        return Sprite.Create(source, spriteRect, pivot, 10.0f, 0, SpriteMeshType.Tight, Vector4.zero, false);
    }

    public void Dispose() => Object.Destroy(container);
}
