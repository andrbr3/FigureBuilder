﻿using Deform;
using HC.GameComponents.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FigureLogicBuilder : MonoBehaviour
{
    [EventListSubscriber("BuildFigureLogic")]
    public SubscribeDataList BuildFigureLogicSubscribe = new SubscribeDataList();

    public event Action<EventParams> OnFigureBuilded;
    [ExecuteListSubscriber("OnFigureBuilded")]
    public ExecuteMethodDataList OnFigureBuildedSubscribe = new ExecuteMethodDataList();

    [SerializeField]
    private GameObject figureBasePrefab;

    private void Start() => EventHelper.Subscribe(this);

    [ActionFunction]
    public void BuildFigureLogic(EventParams param)
    {
        if(!(param is EventParams_GameObject))
        {
            Debug.LogError("Require GO param!");
            return;
        }

        var figureView = ((EventParams_GameObject)param).GetGameObject().transform;
        var figureBase = Instantiate(figureBasePrefab);
        
        var origin = figureBase.FindChildWithTag("FigureViewOrigin");
        var figureRoot = origin? origin : figureBase;
        figureView.SetParent(figureRoot.transform);
        figureView.localPosition = Vector3.zero;

        var deformers = figureBase.GetComponentsInChildren<Deformer>();
        ApplyDeformers(figureView, deformers);

        OnFigureBuilded?.Invoke(new EventParams_GameObject(figureBase, null));
    }

    private void ApplyDeformers(Transform root, IEnumerable<Deformer> deformers)
    {
        foreach(var renderer in root.GetComponentsInChildren<MeshFilter>())
        {
            var deformable = renderer.gameObject.AddComponent<Deformable>();
            foreach(var deformer in deformers)
                deformable.AddDeformer(deformer);
        }
    }
}
