﻿using Boo.Lang;
using HC.GameComponents.Base;
using HCApp;
using System;
using System.Linq;
using UnityEngine;
using Zenject;

public class FigureViewBuilder : MonoBehaviour
{
    [EventListSubscriber("GenerateFigureFromData")]
    public SubscribeDataList GenerateFigureSubscriber = new SubscribeDataList();

    public event Action<EventParams> OnFigureGenerated;
    [ExecuteListSubscriber("OnFigureGenerated")]
    public ExecuteMethodDataList onFigureGeneratedSubscriber;

    [Header("Materials")]
    [SerializeField]
    private Material sides;
    [SerializeField]
    private Material front;
    [SerializeField]
    private float scale = 0.01f;

    public Vector2 boundSize;

    [Inject]
    private FlatMeshGenerator meshGenerator;
    [Inject]
    private IFigureElementProvider elementProvider;

    private void Start() => EventHelper.Subscribe(this);

    [ActionFunction]
    public void GenerateFigureFromData(EventParams param)
    {
        if(!(param is EventParams_FigureData))
        {
            Debug.LogError("param is not figure data!");
            return;
        }
        var figureData = ((EventParams_FigureData)param).FigureData;
        GenerateFigure(figureData);
    }

    public Texture2D ResizeTextureFit(Texture2D texture)
    {
        var textureSize = new Vector2(texture.width, texture.height);
        float textureAspect = textureSize.x/textureSize.y;
        float boundsAspect = boundSize.x/boundSize.y;

        var newSize = (boundsAspect>textureAspect) ?
            new Vector2(boundSize.y * textureAspect, boundSize.y) :
            new Vector2(boundSize.x, boundSize.y / textureAspect);

        var rt = new RenderTexture((int)newSize.x, (int)newSize.y, 24);
        RenderTexture.active = rt;
        Graphics.Blit(texture, rt);
        var result = new Texture2D((int)newSize.x, (int)newSize.y);
        result.ReadPixels(new Rect(0, 0, (int)newSize.x, (int)newSize.y), 0, 0);
        result.Apply();
        return result;
    }

    public void GenerateFigure(FigureDataBase data)
    {
        var texture = ResizeTextureFit(data.FigureTexture);
        var extrudableTexture = new ExtrudableTexture(texture);

        //Generated geometry container
        var figure = new GameObject("figure");
        
        var meshFilter = figure.AddComponent<MeshFilter>();
        meshFilter.sharedMesh = meshGenerator.CreateExtrudedMesh(extrudableTexture);

        //Apply materials
        var meshRenderer = figure.AddComponent<MeshRenderer>();
        meshRenderer.materials = new Material[2] { front, sides };
        meshRenderer.material.mainTexture = texture;

        var figureCollider = figure.AddComponent<MeshCollider>();
        figureCollider.convex = true;
        //figureCollider.isTrigger = true;

        var elementCounter = 0;
        var elementRoot = new GameObject("elementRoot");
        elementRoot.transform.SetParent(figure.transform);

        var elementRootMeshFilter = elementRoot.AddComponent<MeshFilter>();
        elementRoot.AddComponent<MeshRenderer>();
        var elementCoordArray = GetDataElementCoordArray(data);
        var elementPointMesh = meshGenerator.CreatePointsMesh(extrudableTexture, elementCoordArray);
        elementRootMeshFilter.sharedMesh = elementPointMesh;

        SpawnFigureElement(data.mouth);
        foreach(var eyeElement in data.eyes)
            SpawnFigureElement(eyeElement);

        figure.transform.SetParent(transform);
        figure.transform.localPosition = Vector3.zero;
        figure.transform.localScale = Vector3.one * scale;

        var spawnedObjectParams = new EventParams_GameObject(figure, null);
        OnFigureGenerated?.Invoke(spawnedObjectParams);

        extrudableTexture.Dispose();

        void SpawnFigureElement(FigureStickerElement element)
        {
            var spawnedObject = Instantiate(elementProvider.GetElement(element.id));
            spawnedObject.transform.SetParent(elementRoot.transform);

            var v2Pos = extrudableTexture.GetPosFromUVCoord(element.Pos);
            var elementPos = new Vector3(v2Pos.x, v2Pos.y, -(meshGenerator.width+0.01f));

            spawnedObject.transform.localPosition = elementPos;

            var follower = spawnedObject.AddComponent<MeshPointFollower>();
            follower.target = elementRootMeshFilter;
            follower.pointId = elementCounter++;
        }
    }

    private Vector2[] GetDataElementCoordArray(FigureDataBase data)
    {
        var coordList = new List<Vector2>();
        coordList.Add(data.mouth.Pos);
        coordList.AddRange(data.eyes.Select(element => element.Pos));
        return coordList.ToArray();
    }
}
