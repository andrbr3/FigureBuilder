﻿using Boo.Lang;
using System.Linq;
using UnityEngine;

public class FlatMeshGenerator : MonoBehaviour
{
    public float width = 1f;
    public float figureElementIndent = 1f;
    
    public Mesh CreateExtrudedMesh(ExtrudableTexture extrudableTexture)
    {
        width = Mathf.Max(width, 0);

        // convert polygon to triangles
        var path = extrudableTexture.GetMainPath();
        
        var vertices = new Vector3[path.Length * 2];
        var uvs = new Vector2[path.Length * 2];

        for(int i = 0;i < path.Length;i++)
        {
            // front vertex
            vertices[i] = path[i];
            vertices[i].z = -width;
            uvs[i] = extrudableTexture.GetSpriteCoordinate(path[i]);
            // back vertex 
            vertices[i + path.Length] = path[i];
            vertices[i + path.Length].z = width;
            uvs[i + path.Length] = uvs[i];
        }

        var triangulator = new MeshTriangulator(path);
        int[] tris = triangulator.Triangulate();

        // triangles on extrude faces
        int[] faceTriangles = new int[tris.Length * 2];

        // front triangles
        for(int i = 0;i < tris.Length;i += 3)
        {
            faceTriangles[i] = tris[i];
            faceTriangles[i + 1] = tris[i + 1];
            faceTriangles[i + 2] = tris[i + 2];
        }

        // back triangles
        for(int i = 0;i < tris.Length;i += 3)
        {
            faceTriangles[tris.Length + i] = tris[i + 2] + path.Length;
            faceTriangles[tris.Length + i + 1] = tris[i + 1] + path.Length;
            faceTriangles[tris.Length + i + 2] = tris[i] + path.Length;
        }

        // triangles around the perimeter of the object
        int[] perimeterTriangles = new int[path.Length * 6];
        for(int i = 0;i < path.Length;i++)
        {
            int n = (i + 1) % path.Length;
            perimeterTriangles[i*6] = i;
            perimeterTriangles[i * 6 + 1] = n;
            perimeterTriangles[i * 6 + 2] = i + path.Length;
            perimeterTriangles[i * 6 + 3] = n;
            perimeterTriangles[i * 6 + 4] = n + path.Length;
            perimeterTriangles[i * 6 + 5] = i + path.Length;
        }

        var mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.subMeshCount = 2;
        mesh.SetTriangles(faceTriangles, 0);
        mesh.SetTriangles(perimeterTriangles, 1);
        mesh.uv = uvs;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        return mesh;
    }

    public Mesh CreatePointsMesh(ExtrudableTexture extrudableTexture, Vector2[] points)
    {
        var vertices = new List<Vector3>();
        foreach(var uvPoint in points)
        {
            var flatPoint = extrudableTexture.GetPosFromUVCoord(uvPoint);
            vertices.Add(new Vector3(flatPoint.x, flatPoint.y, - (width+figureElementIndent)));
        }
        var mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        return mesh;
    }
}
